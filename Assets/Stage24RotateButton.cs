﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Stage24RotateButton : ButtonBase
{
    private int _Num;
    public int Num
    {
        get { return _Num;}
        set {
            if (value > 17)
            {
                value = 0;
            }
            _Num = value;
        }
    }
    private void OnEnable()
    {
        transform.rotation=Quaternion.Euler(Vector3.zero);
        Num = 0;
    }

    protected override void OnBtnClick()
    {
        Num++;
        transform.DORotate(transform.eulerAngles + new Vector3(0, 0, -20), 0.2f, RotateMode.FastBeyond360)
            .OnPlay(() => { GameManager.gameManager.SetInputActive(false); })
            .OnComplete(() =>
            {
                GameManager.gameManager.SetInputActive(true);
            });
    }

    void ButtonRotationInit()
    {
        transform.DORotate(transform.eulerAngles - new Vector3(0, 0, -20*Num), 0.5f,
            RotateMode.FastBeyond360) .OnPlay(() =>
            {
                GameManager.gameManager.SetInputActive(false);
            })
            .OnComplete(() =>
            {
                GameManager.gameManager.SetInputActive(true);
            });
        Num = 0;
    }
}
