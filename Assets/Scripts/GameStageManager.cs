﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameStageManager 
{

    public static int StageNum
    {
        set
        {
            PlayerPrefs.SetInt("StageNum", value);
        }
        get
        {
            return PlayerPrefs.GetInt("StageNum", 79);
        }
    }
    
}
