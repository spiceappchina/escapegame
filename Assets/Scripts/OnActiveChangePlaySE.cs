﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnActiveChangePlaySE : MonoBehaviour
{
        public TipShowType PlayType;
        public SEClipName Se;
        public float DelayTime;
        
        private void OnEnable()
        {
                if (PlayType!=TipShowType.OnEnable)
                {
                        return;
                }
                Invoke(nameof(PlaySe),DelayTime);
        }

        private void OnDisable()
        {
                if (PlayType!=TipShowType.OnDisable)
                {
                        return;
                }
                Invoke(nameof(PlaySe),DelayTime);
        }

        void PlaySe()
        {
                AudioManager.audioManager.EnvironmentalSoundPlay(Se);  
        }
}
