﻿
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;


public class Menu : MonoBehaviour
{
    public static Menu menu;
    public List<GameObject> Pages;
    private int _PageNum=0;
    private List<Button> StageBtns;
    private List<Text> StageBtnTexts;
    public int StageAmount=79;
    public int PageNum
    {
        get { return _PageNum;}
        set {
            if (Pages!=null)
            {
                if (value < 0)
                {
                value = Pages.Count - 1;
                }
                if (value > Pages.Count - 1)
                {
                value =0;
                }
                _PageNum = value;
                foreach (var VARIABLE in Pages)
                {
                    VARIABLE.SetActive(false);
                }
                Pages[value].SetActive(true);
            }
        }
    }
    private void Awake()
    {
        menu = this;
    }

    void Start()
    {
        InitPages();
    }

    private void InitPages()
    {
        StageBtns=new List<Button>();
        StageBtnTexts=new List<Text>();
       
        var UnLockStage = GameStageManager.StageNum;
        if (UnLockStage > StageAmount)
        {
            UnLockStage = StageAmount;
        }
        foreach (var VARIABLE in Pages)
        {
            VARIABLE.SetActive(false);
            foreach (var Buttons in VARIABLE.GetComponentsInChildren<Button>())
            {
                Buttons.enabled = false;
                StageBtns.Add(Buttons);
                
            }
            foreach (var Texts in  VARIABLE.GetComponentsInChildren<Text>())
            {
                var tempColor = Texts.color;
                Texts.color = new Color(tempColor.r, tempColor.g, tempColor.b, 0);
                Texts.raycastTarget = false;
                StageBtnTexts.Add(Texts);
                
            }
        }
        for (int i = 1; i-1 < UnLockStage; i++)
        {
            StageBtns[i - 1].image.color=Color.clear;
            StageBtns[i - 1].image.raycastTarget = true;
            StageBtns[i - 1].enabled = true;
            StageBtns[i - 1].gameObject.AddComponent<SelectStage>().StageName=$"Stage{i}";
            StageBtns[i - 1].gameObject.AddComponent<PlaySE>().Se = SEClipName.UIBTN;
            StageBtns[i - 1].gameObject.AddComponent<ButtonEffect>()._text =
                StageBtns[i - 1].gameObject.GetComponentInChildren<Text>();
            // StageBtnTexts[i - 1].text = i.ToString();
            var tempColor = StageBtnTexts[i - 1].color;
            StageBtnTexts[i - 1].color =new Color(tempColor.r, tempColor.g, tempColor.b,1);
            StageBtnTexts[i - 1].fontStyle = FontStyle.Bold;
        }
        Pages[0].SetActive(true);
    }
}
