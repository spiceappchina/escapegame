﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectStage : UIButtonBase
{
    public string StageName;
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        Application.LoadLevel(StageName);
    }
}
