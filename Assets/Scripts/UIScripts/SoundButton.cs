﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SoundButton : UIButtonBase, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    
    public Sprite[] Sprites;
    private bool PressTimes=true;
    private List<AudioSource> AudioSources;


    private bool IsDown;
    private float Delay = 2f;//延迟相当于按下持续时间
    private float LastDownTime;
    private GameObject GameWinPanel;
    protected override void Start()
    {
        base.Start();
        AudioSources=new List<AudioSource>();
        AudioSources.Add( AudioManager.audioManager.BGMPlayer);
        AudioSources.Add( AudioManager.audioManager.SEPlayer);
        AudioSources.Add( AudioManager.audioManager.EnvironmentalSoundPlayer);
       

    }
   
    private void Update()
    {
        //判断是否属于长按
        if (IsDown)
        {
            if (Time.time - LastDownTime >= Delay)
            {


                 LastDownTime = Time.time;
            }
        }
    }
    /// <summary>
    /// 单击按钮一次
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {

        IsDown = true;
        LastDownTime = Time.time;
    }
    /// <summary>
    /// 按钮抬起
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerUp(PointerEventData eventData)
    {

        IsDown = false;
    }
    /// <summary>
    /// 鼠标离开按钮区域
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {

        IsDown = false;
    }
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        PressTimes = !PressTimes;
        foreach (var VARIABLE in AudioSources)
        {
            VARIABLE.mute = !PressTimes;
        }
        int temp = Convert.ToInt32(PressTimes);
        gameObject.GetComponent<Image>().sprite = Sprites[temp];
    }

}