﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class  ItemDescriptionPanel: MonoBehaviour
{
    public GameObject Black; 
    [NonSerialized] public Image BlackImage;
    private Sprite _BlackSprite;
   public Sprite BlackSprite
    {
    
        get { return _BlackSprite;}

        set {
            if (value != null)
            {
                _BlackSprite = value;
              
                    OnSpriteChange();
                

               
            }
        

    }
    }


    private void Start()
    {
        
    }

    private void Awake()
    {
            
       
    }

    private void OnSpriteChange()
    {
        BlackImage=Black.GetComponent<Image>();
        var BlackButton= Black.GetComponent<BlackButton>();
        BlackImage.sprite = BlackSprite;
        
    }
}
