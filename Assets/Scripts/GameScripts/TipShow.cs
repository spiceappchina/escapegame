﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipShow : MonoBehaviour
{
    private List<bool> Tips => GameManager.gameManager.PasswordTips;
    public int PasswordTipIndex;
    public TipShowType TipShowType;
    // Start is called before the first frame update
    private void OnDisable()
    {
        
        if (TipShowType==TipShowType.OnEnable)
        {
            return;
        }

        if (Tips != null && Tips.Count > 0)
        {
            GameManager.gameManager.PasswordTips[PasswordTipIndex - 1] = true;
            
        }
    }

    private void OnEnable()
    {

        if (TipShowType==TipShowType.OnDisable)
        {
            return;
        }

        if (Tips != null && Tips.Count > 0)
        {
            GameManager.gameManager.PasswordTips[PasswordTipIndex - 1] = true;
            
        }
    }
    
}

public enum TipShowType
{
    OnEnable,
    OnDisable
}
