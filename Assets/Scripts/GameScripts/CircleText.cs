﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
 
public class CircleText : MonoBehaviour
{
    public Transform CenterPoint;
    public string Txt;
    public int TextPointNum;
    public int  Radius;
    public Text TextPoint;
    private List<Text> texts;
    private void Start()
    {     
        if (TextPointNum<=0)
        {
            TextPointNum = 1;
        }

        while(((360/TextPointNum)%1)!=0)
        {
            TextPointNum++;
        }

		
        texts=new List<Text>();
        for (int i = 0; i < TextPointNum; i++)
        {
            var point=Instantiate(TextPoint, transform);
            point.transform.localPosition=new Vector3(-Radius,0,0);
            point.transform.RotateAround(CenterPoint.position, Vector3.forward, i*(360/TextPointNum));
            point.text = Txt[i % (Txt.Length)].ToString();
            texts.Add(point);
        }

        UpdateTextDirection();
    }

    public void UpdateTextDirection()
    {

        foreach (var text in texts)
        {
			
            text.transform.eulerAngles=Vector3.zero;
            continue;
            var textTargetScale = Vector3.one;
            var vector = (text.transform.position - transform.position).normalized;
            if (Vector3.Angle(vector,Vector3.left)<=90)
            {
                textTargetScale.y = 1;
                textTargetScale.y = 1;
            }
            else
            {
                textTargetScale.x = -1;
                textTargetScale.y = -1;
            }
            text.transform.localScale = textTargetScale;
        }
    }
}