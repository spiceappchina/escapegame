﻿using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    private void OnEnable()
    {
        transform.parent.DOLocalMoveX(240, 6f).
            OnStart(() => { GameManager.gameManager.SetInputActive(false); }).
            OnComplete(() => { GameManager.gameManager.SetInputActive(true); });
    }

}
