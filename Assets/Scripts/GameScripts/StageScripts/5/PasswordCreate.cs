﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasswordCreate : MonoBehaviour
{
    public static PasswordCreate instance;
    public List<Transform> point;
    public password Password0;
    public password Password1;
    public password Password2;
    public List<GameObject> lights;
    private int curtenIndex;
    public PasswordInspectorBase inspectorBase;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CreateNewPassword();
    }

    // Update is called once per frame
    void Update()
    {
     
    }
  public  void CreateNewPassword() 
    {
        string PasswordText="";
        var a=Random.Range(0, point.Count - 1);
        Password0.transform.position = point[a].position;
        var b = Random.Range(0, point.Count - 1);
        while (b==a)
        {
         b=Random.Range(0, point.Count - 1);
        }
        Password1.transform.position = point[b].position;
        var c = Random.Range(0, point.Count - 1);
        while (c == a||c==b)
        {
            c = Random.Range(0, point.Count - 1);
        }
        Password2.transform.position = point[c].position;
        int[] abc = { a, b, c };
        for (int i = 0; i < abc.Length; i++)
        {
            for (int j = 0; j < abc.Length-1; j++)
            {
                if (abc[j]>abc[j+1]) 
                {
                    var n = abc[j + 1];
                    abc[j + 1] = abc[j];
                    abc[j] = n;
                }
            }
        }
        foreach (var item in abc)
        {
            if (item == a) 
            {
                PasswordText += Password0.passwordStr;
            }
            if (item == b)
            {
                PasswordText += Password1.passwordStr;
            }
            if (item == c) 
            {
                PasswordText += Password2.passwordStr;
            }
        }
        inspectorBase.StringPassword = PasswordText;
    }
    private void OnDisable()
    {
        CreateNewPassword();
    }
}

[System.Serializable]
public struct password
{
  public  Transform transform;
  public  string passwordStr;
}