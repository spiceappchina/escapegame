﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage5Password : PasswordInspectorBase
{
    private int index;
    PasswordCreate instance => PasswordCreate.instance;
    protected override void OnPasswordError()
    {
        base.OnPasswordError();
        foreach (var item in instance.lights)
        {
            item.SetActive(false);
        }
        instance.CreateNewPassword();
        index = 0;
    }
    protected override void OnPasswordYes()
    {
        base.OnPasswordYes();
        instance.lights[index].SetActive(true);
        index++;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        if (index<instance.lights.Count) 
        {
            OnPasswordError();
        }
    }

}
