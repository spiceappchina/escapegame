﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LeafRotateButton : InputButton
{
   public int ControlIndex;
   private int _Num;
    public int mluate =-1;
   private int Num
   {
      get { return _Num;}
      set {
         if (value > 3)
         {
            value = 0;
         }
         _Num = value;
      }
   }
   public int RightNum;
   protected override void Start()
   {
      base.Start();
        Btn.image.alphaHitTestMinimumThreshold = 0.5f;
      //_passwordInspectorBase.BoolArrayPassword[ControlIndex] = Num == RightNum;
   }
   protected override void OnBtnClick()
   {
      Num++;
         transform.DORotate(transform.eulerAngles + new Vector3(0, 0, mluate*90), 0.2f, RotateMode.FastBeyond360)

            .OnPlay(
             () => {
                GameManager.gameManager.SetInputActive(false);
                 if (transform.childCount!=0) 
                 {
                     for (int i = 0; i < transform.childCount; i++)
                     {
                         var child = transform.GetChild(i);
                         child.Rotate(new Vector3(0, 0, -90));
                     }
                 }
            }
            )
            .OnComplete(() =>
            {
               GameManager.gameManager.SetInputActive(true);
               _passwordInspectorBase.BoolArrayPassword[ControlIndex] = Num == RightNum;
               _passwordInspectorBase.Length++;
            });
      
   }

   private void OnDisable()
   {
     // OnBecameInvisible();
        transform.eulerAngles = Vector3.zero;
        Num = 0;
        _passwordInspectorBase.BoolArrayPassword[ControlIndex] = Num == RightNum;
        if (transform.childCount != 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                child.rotation = Quaternion.identity;
            }
        }
    }

   private void OnBecameInvisible()
   {
      
   }

   private void OnEnable()
   {
      Invoke(nameof(OnBecameVisible),0);
     
   }

   private void OnBecameVisible()
   {
      transform.eulerAngles=Vector3.zero;
       Num = 0;
       _passwordInspectorBase.BoolArrayPassword[ControlIndex] = Num == RightNum;
   }
}
