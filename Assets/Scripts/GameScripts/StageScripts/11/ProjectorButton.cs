﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectorButton : TakeEffectWithTipButton
{
    private int PressTimes;
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        PressTimes++;
    }

    private void OnDisable()
    {
        if (GameManager.gameManager.PasswordTips[TipIndex - 1])
        {
             return;
        }
        if (PressTimes % 2 == 1)
        {
            OnBtnClick();
        }
    }
}
