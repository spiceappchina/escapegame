﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class CarMove : MonoBehaviour
{
    public List<Transform> MovePath;
    public float WaitTime = 0;
    public SEClipName MoveSE;
    public GameObject Next;
    private Vector3[] path;
    private int i;
    void Start()
    {
       
        path = new Vector3[MovePath.Count];
        for (int i = 0; i < MovePath.Count; i++)
        {
            path[i] = MovePath[i].position;
        }
        Move();
    }
    void Move()
    {

        GameManager.gameManager.SetBackButtonActive(false);
        transform.DOPath(path, 1)
            .SetDelay(WaitTime)
            .OnStart(() => { AudioManager.audioManager.SEPlay(MoveSE); })
          .OnWaypointChange(WaypointChange)
            .OnComplete(EnableNext);
    }

    void WaypointChange(int n)
    {
        transform.eulerAngles = MovePath[this.i].rotation.eulerAngles;
        if (i<MovePath.Count-1) { this.i++; }
        
    }

    void EnableNext()
    {
        GameManager.gameManager.SetBackButtonActive(true);
        Next.SetActive(!Next.activeSelf);
       
    }
}