﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeEffectWithTipButton : ButtonBase
{
    public int TipIndex;
    public List<GameObject> TakeEffectControls;

    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        if (GameManager.gameManager.PasswordTips[TipIndex - 1])
        {
            foreach (var VARIABLE in TakeEffectControls)
            {
                VARIABLE.SetActive(!VARIABLE.activeSelf);
            }
        }
    }
}
