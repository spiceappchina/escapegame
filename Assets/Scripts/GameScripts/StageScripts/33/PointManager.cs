﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointManager : MonoBehaviour
{
    
    public static PointManager instance;
    public int DestoryIndex;
    public List<GameObject> Points;

    private List<bool> Actives ;
    public GameObject[] ManagedObjs;
    public GameObject[] SetTrueManagedObjs;
    public GameObject[] SetFalseManagedObjs;
    private bool Over = false;
  
    private GameManager gameManger => GameManager.gameManager;
    private void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {
        Actives=new List<bool>();
        if (!Over)
        {
            foreach (var VARIABLE in Points)
            {
                VARIABLE.SetActive(false);
            }
        }
    }

    public void Check()
    {
        if (Over) return;
        Actives.Clear();
        foreach (var VARIABLE in Points)
        {
            Actives.Add(VARIABLE.activeSelf);
        }

        if (!Actives.Contains(false))
        {
            if (gameManger.Items[DestoryIndex - 1] == null) return;
            Over = true;
            foreach (var VARIABLE in ManagedObjs)
            {
                VARIABLE.SetActive(!VARIABLE.activeSelf);
            }
            foreach (var VARIABLE in SetFalseManagedObjs)
            {
                VARIABLE.SetActive(false);
                GameManager.gameManager.ObjList.Remove(VARIABLE);
            }
            foreach (var VARIABLE in SetTrueManagedObjs)
            {
                VARIABLE.SetActive(true);
            }
            DestroyImmediate(gameManger.Items[DestoryIndex - 1]);
        }
    }
}
