﻿
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Stage36Btn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image _text;
    public void OnPointerDown(PointerEventData eventData)
    {

        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 1.0f);


    }

    public void OnPointerUp(PointerEventData eventData)
    {

        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 0);

    }
}
