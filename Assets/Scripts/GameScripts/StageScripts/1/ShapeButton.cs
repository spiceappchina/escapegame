﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeButton : UIButtonBase
{
    public int ThisButtonIndex;
    public string Expression;
    private  ShapePanel ShapePanel=>ShapePanel.shapePanel;
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        if (!ShapePanel.ButtonLock[ThisButtonIndex])
        {
            transform.position = ShapePanel.TargetPos[ShapePanel.ShapeIndex].position;
            ShapePanel.ButtonLock[ThisButtonIndex] = true;
            ShapePanel.ShapeIndex++;
            ShapePanel.Txt += Expression;
        }
           
    }
}
