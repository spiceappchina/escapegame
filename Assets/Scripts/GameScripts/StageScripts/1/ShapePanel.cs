﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapePanel : PasswordInspectorBase
{
    public int ButtonCount = 4;
    public static ShapePanel shapePanel;
    public List<Transform> TargetPos;
    public int ShapeIndex=0;
    public List<bool> ButtonLock;
    public List<Transform> Buttons;
    public List<Transform> ButtonInitPos;
    private void Awake()
    {
        if (shapePanel== null)
        {
            shapePanel = this;
        }

    }

    protected override void Start()
    {
        base.Start();
        InitButton();
    }


    void InitButton()
    {
        ButtonLock=new List<bool>();
        for (int i = 0; i < ButtonCount; i++)
        {
            ButtonLock.Add(false);
            Buttons[i].position = ButtonInitPos[i].position;
           
        }
    }    

    protected override void OnPasswordError()
    {
        base.OnPasswordError();
        ShapeIndex = 0;
        InitButton();
    }
    
}
