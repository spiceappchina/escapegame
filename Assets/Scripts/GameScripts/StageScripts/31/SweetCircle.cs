﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SweetCircle : MonoBehaviour
{
    public CircleColor ThisColor;
    public List<Transform> PosPoints;
    private CirclePosInfo index;
    private Vector3 info;
    private bool InitSwitch=false;
    private bool SpecialInitSwitch=true;
    private bool PasswordYes = false;
    public  void ReGetPos() 
    {
        
        bool v = HanoiManager.Instance.CirclePosDict.TryGetValue(ThisColor, out index);
        if (v) 
        {
            var temp = index.ColumnIndex*5+index.OrderIndex;
            transform.position = PosPoints[temp].position;
        }
    }

    private void Start()
    {
        info = transform.position;
        InitSwitch = true;
    }

    private void OnEnable()
    {
        if (PasswordYes)
        {
            return;
        }
        if (InitSwitch)
        {
            transform.position= info;
        }


    }
    private void OnCircleDrop()
    {
        
        if (ThisColor==CircleColor.GREEN&&SpecialInitSwitch)
        {
            ReGetPos();
            info = transform.position;
            InitSwitch = true;
            SpecialInitSwitch = false;
           
        }
    }

    private void PasswordYesOver()
    {
        PasswordYes = true;
    }
}
