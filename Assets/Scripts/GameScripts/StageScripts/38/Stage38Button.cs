﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Stage38Button : ButtonEffect
{
    public GameObject obj;
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        obj.SetActive(!obj.activeSelf);
    }
    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        obj.SetActive(!obj.activeSelf);
    }
}
