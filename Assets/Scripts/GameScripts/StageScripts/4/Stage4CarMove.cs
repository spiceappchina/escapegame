﻿
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class Stage4CarMove : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Transform car;
    float Y;
    private Vector3 initPos;
    public Transform mouse;
    private Transform oldfather;
    public GameObject[] TheControlGameobjectsDontJoinOBJ;
    private void Start()
    {
       
        initPos = car.position;
        oldfather = car.parent;
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {

        car.SetParent(mouse);
        if (TheControlGameobjectsDontJoinOBJ != null)
        {
            foreach (var VARIABLE in TheControlGameobjectsDontJoinOBJ)
            {
                VARIABLE.SetActive(!VARIABLE.activeSelf);
            }
        }

    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
       
        car.SetParent(oldfather);
        car.DOMove(initPos, 0.2f);
        
    }
    private void Update()
    {
       // new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
        mouse.position =new Vector3(mouse.position.x, Input.mousePosition.y);
    }
}
