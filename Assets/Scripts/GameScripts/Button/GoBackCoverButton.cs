﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoBackCoverButton : UIButtonBase
{
    private Platform ThisPlatform => GameManager.gameManager.ThisStagePlatform;
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        switch (ThisPlatform)
        {
            case Platform.WebGl:
                // SceneManager.LoadScene(Application.loadedLevelName);
                Application.LoadLevel("Cover");
                break;
            case Platform.App:
                Application.LoadLevel("Cover");
                break;
        }
    }
}


