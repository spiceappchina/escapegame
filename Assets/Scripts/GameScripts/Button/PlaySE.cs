﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySE : UIButtonBase
{
    public SEClipName Se;
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        AudioManager.audioManager.SEPlay(Se);
       
    }
}
