﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoBackButton : ButtonBase
{
     List<GameObject> ObjList => GameManager.gameManager.ObjList;
     public static GoBackButton goBackButton;

     private void Awake()
     {
         if (goBackButton==null)
         {
             goBackButton = this;
         }
     }
    protected override void Start()
    {
        base.Start();
        if (gameObject.tag == "GoBack") 
        {
            var rectTransform = gameObject.GetComponent<RectTransform>();
            rectTransform.pivot = Vector2.zero;
            rectTransform.anchorMax =new Vector2(0.5f, 0.5f);
            rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
            var x = gameManger.transform.position.x;
            var y = gameManger.transform.position.y;
            var scale = transform.parent.localScale;
            rectTransform.position = new Vector2(x-(scale.x*960), y-(scale.y*540));
        }
    }
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        if (ObjList.Count > 0)
        {
            
            var temp=ObjList[ObjList.Count-1]; 
            temp.SetActive(!temp.activeSelf);
            ObjList.RemoveAt(ObjList.Count-1);
            GameManager.gameManager.SetBackButtonActive();
        }
        else
        {
            Application.LoadLevel("StageSelect");
        }

    }

    public  void GoBack()
    {
        OnBtnClick();
    }

    public void GoBack(float DelayTime)
    {
        GameManager.gameManager.SetInputActive(false);
        Invoke(nameof(OnBtnClick),DelayTime);
        Invoke(nameof(SetInputActiveTrue),DelayTime);
    }

    private void SetInputActiveTrue()
    {
        GameManager.gameManager.SetInputActive(true);
    }
    
}
