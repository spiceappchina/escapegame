﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;



public class RotateButton : ButtonBase
{
    public Transform FigurePlate;
    public int CircleIndex;
    public int RightAngle;
    private float angle;
    public Transform Axis;
    private GameManager GameManager => GameManager.gameManager;
    [SerializeField] private CircleText CircleText;
    protected override void Start()
    {
        base.Start();       
        angle = 360f / transform.parent.GetComponent<CircleText>().TextPointNum; //根据Circle中每圈字符个数和360度进行平分计算得到每次点击按钮应该转动的角度
        Btn.GetComponent<Image>().alphaHitTestMinimumThreshold = 0.1f;
       var range= UnityEngine.Random.Range(1, transform.parent.GetComponent<CircleText>().TextPointNum - 1);
        transform.parent.transform.eulerAngles=new Vector3(0,0,range*angle); 
        
    }

    protected override void OnBtnClick()
    {
        var endValue = transform.parent.transform.eulerAngles + new Vector3(0, 0, angle);
        if (Input.mousePosition.y > Axis.position.y)
        {
            endValue = transform.parent.transform.eulerAngles - new Vector3(0, 0, angle);
        }

        transform.parent.transform.DORotate(endValue, 0.2f, RotateMode.FastBeyond360)
            .OnPlay(() => { GameManager.SetInputActive(false); })
            .OnUpdate(() => { CircleText.UpdateTextDirection(); })
            .OnComplete(() =>
            {
                CircleText.UpdateTextDirection();
                GameManager.SetInputActive(true);
                FigurePlate.GetComponent<CheckPassword>().Password[CircleIndex - 1] =
                    RightAngle - 1 <= Math.Ceiling((Mathf.Abs(transform.parent.transform.eulerAngles.z)))
                    && Math.Ceiling((Mathf.Abs(transform.parent.transform.eulerAngles.z))) <= RightAngle + 1;
            });
    }

   
}