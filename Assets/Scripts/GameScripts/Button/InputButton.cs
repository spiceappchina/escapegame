﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InputButton : ButtonBase
{
   public GameObject PasswordInspector;
   public Text Inspector;
   public String InputStr;
   protected PasswordInspectorBase _passwordInspectorBase;
   protected bool ContainText;

   private void Awake()
   {
      ContainText = Inspector != null;
      if (ContainText)
      {
         _passwordInspectorBase=Inspector.GetComponent<PasswordInspectorBase>();
      }
      else
      {
         _passwordInspectorBase=PasswordInspector.GetComponent<PasswordInspectorBase>();
      }
   }
   protected override void OnBtnClick()
   {
      base.OnBtnClick();
      AfterBtnClick();
   }

   protected virtual void AfterBtnClick()
   {
      switch (_passwordInspectorBase.PasswordType)
      {
         case PasswordType.String:
            _passwordInspectorBase.Txt += InputStr;
            break;
         case PasswordType.BoolArray:
            
            break;
      }
     
      _passwordInspectorBase.Length++;
   }
   
}
