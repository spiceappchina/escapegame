﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NextStage : UIButtonBase
{
    private int ThisStageNum;
    public BGM Bgm;
    protected override void Start()
    {
        base.Start();
     var  RectTransform= gameObject.GetComponent<RectTransform>();
        RectTransform.anchorMax = new Vector2(0.5f, 0.5f);
        RectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        RectTransform.pivot = new Vector2(0.5f, 0.5f);
      //  RectTransform.position = Vector3.zero;
       var image= RectTransform.GetComponent<Image>();
        image.SetNativeSize();
        AudioManager.audioManager.SelectBGM(Bgm);
         ThisStageNum=int.Parse(Application.loadedLevelName.Remove(0,5));

        if (ThisStageNum>= GameStageManager.StageNum)
        {
            GameStageManager.StageNum++;
        }
       

        GoBackButton.goBackButton.gameObject.SetActive(false);
        
    }
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        SceneManager.LoadScene(Application.loadedLevelName);
        try
        {
            Application.LoadLevel($"Stage{ThisStageNum + 1}");
        }
        catch (System.Exception)
        {
            Application.LoadLevel($"Cover");
            throw;
        }
       
    }
}
