﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetItemWithItemButton : ButtonBase
{
    public GameObject ItemDescriptionPanel;
    private GameManager temp => GameManager.gameManager;
    public GameObject Item;
    public bool DoGoback = true;
    public float DelayTime;
    public SEClipName TheControlsOpen;
    public SEClipName Succeed;
    public SEClipName Fail;
    public GameObject BgChange;
    protected bool BgChangeFinish = false;
    public int NeedItemIndex;
    private GameManager gameManger => GameManager.gameManager;
    protected bool enabled = false;
    private bool Lock => GameManager.gameManager.ItemSelecteditions[NeedItemIndex - 1]; //获取到对应的道具激活状态 
    public bool DoGoBack = false;
    protected override void OnBtnClick()
    {

        if (Lock || enabled)
        {
            BeforeTheControlsOpen();
            if (BgChange != null&&!BgChangeFinish)
            {
                BgChange.SetActive(!BgChange.activeSelf);
                BgChangeFinish = true;
            }
            if (!enabled)
            {
            var WhetherDestory =gameManger.Items[NeedItemIndex - 1].GetComponent<ItemButton>().Reusable;
            if (gameManger.Items[NeedItemIndex - 1] != null&&!WhetherDestory)
            {
                DestroyImmediate(gameManger.Items[NeedItemIndex - 1]);
                enabled = true;
            }
            else
            {
                gameManger.Items[NeedItemIndex - 1].GetComponent<ItemButton>().AfterUse(); 
            }
            }
            
            AudioManager.audioManager.SEPlay(Succeed);
        }
        else
        {
            OnTheControlsOpenFail();
        }


    }

    protected virtual void BeforeTheControlsOpen()
    {
        gameManger.SetInputActive(false);

        Invoke(nameof(OnTheControlsOpen), DelayTime);
    }

    private void OnTheControlsOpen()
    {
        AudioManager.audioManager.SEPlay(TheControlsOpen);
        base.OnBtnClick();
        gameManger.SetInputActive(true);
        foreach (var VARIABLE in temp.Girds)
        {
            if (VARIABLE.transform.childCount == 0)
            {
                VARIABLE.GetComponent<Image>().color=Color.white;
                var TempItem =Instantiate(Item,VARIABLE.transform);
                TempItem.GetComponent<ItemButton>().ItemDescriptionPanel = ItemDescriptionPanel;
                if (DoGoback)
                {
                    temp.ObjList.RemoveAt(temp.ObjList.Count-1);
                }
                var Index = TempItem.GetComponent<ItemButton>().ItemSelectedIndex-1;
                temp.Items[Index] = TempItem;
                transform.parent.gameObject.SetActive(false);
                return;
            }
        }
        
        if (DoGoBack)
        {
            GoBackButton.goBackButton.GoBack();
        }
    }

    protected virtual void OnTheControlsOpenFail()
    {
        AudioManager.audioManager.SEPlay(Fail);
    }

}
