﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheDoorOpen : TriggerButton
{
    protected override void BeforeTheControlsOpen()
    {
        base.BeforeTheControlsOpen();
        GoBackButton.goBackButton.GoBack();
    }
}
