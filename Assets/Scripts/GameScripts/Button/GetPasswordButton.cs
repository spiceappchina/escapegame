﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPasswordButton : ButtonBase
{
    private List<Boolean> Tips => GameManager.gameManager.PasswordTips;
    public int PasswordTipIndex;
    protected override void OnBtnClick()
    {
        base.OnBtnClick();
        if (Tips != null && Tips.Count > 0)
        {
            GameManager.gameManager.PasswordTips[PasswordTipIndex - 1] = true;
        }
       
    }

    protected override void Start()
    {
        base.Start();
        OnBtnClick();
    }
}
