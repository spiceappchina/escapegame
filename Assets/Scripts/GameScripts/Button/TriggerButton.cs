﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class TriggerButton : ButtonBase
{
    public GameObject[] TheControlsDontJoinOBJ;
    public float DelayTime;
    public SEClipName TheControlsOpen;
    public SEClipName Succeed;
    public SEClipName Fail;
    public GameObject BgChange;
    protected bool BgChangeFinish = false;
    public int NeedItemIndex;
    public bool EveryTimeNeedItem;
    private GameManager gameManger => GameManager.gameManager;
    protected bool enabled = false;
    public bool GoBackDelay;
    public float GoBackDelayTime; 
    private bool Lock => GameManager.gameManager.ItemSelecteditions[NeedItemIndex - 1]; //获取到对应的道具激活状态 
    public bool DoGoBack = false;
    protected override void OnBtnClick()
    {

        if (Lock || enabled)
        {
            BeforeTheControlsOpen();
            if (BgChange != null&&!BgChangeFinish)
            {
                BgChange.SetActive(!BgChange.activeSelf);
                BgChangeFinish = true;
            }

            if (!enabled)
            {
                var WhetherDestory =gameManger.Items[NeedItemIndex - 1].GetComponent<ItemButton>().Reusable;
                if (gameManger.Items[NeedItemIndex - 1] != null&&!WhetherDestory)
                {
                    DestroyImmediate(gameManger.Items[NeedItemIndex - 1]);
                    if (!EveryTimeNeedItem)
                    {
                        enabled = true;
                    }

                   
                }
                else
                {
                    gameManger.Items[NeedItemIndex - 1].GetComponent<ItemButton>().AfterUse();
                }
            }
            AudioManager.audioManager.SEPlay(Succeed);
        }
        else
        {
            OnTheControlsOpenFail();
        }


    }

    protected virtual void BeforeTheControlsOpen()
    {
        gameManger.SetInputActive(false);

        Invoke(nameof(OnTheControlsOpen), DelayTime);
    }

    private void OnTheControlsOpen()
    {
        AudioManager.audioManager.SEPlay(TheControlsOpen);
        base.OnBtnClick();
        if (TheControlsDontJoinOBJ!=null)
        {
            foreach (var VARIABLE in TheControlsDontJoinOBJ)
            {
                
                VARIABLE.SetActive(!VARIABLE.activeSelf);
                GameManager.gameManager.ObjList.Remove(VARIABLE);
            }
            gameManger.SetBackButtonActive();
        }
        gameManger.SetInputActive(true);
        if (DoGoBack)
        {
            if (GoBackDelay)
            {
                GoBackButton.goBackButton.GoBack(GoBackDelayTime);
            }
            else
            {
                GoBackButton.goBackButton.GoBack();
            }
        }
    }

    protected virtual void OnTheControlsOpenFail()
    {
        AudioManager.audioManager.SEPlay(Fail);
    }

}
