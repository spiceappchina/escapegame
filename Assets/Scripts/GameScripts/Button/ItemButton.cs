﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ItemButton : ButtonBase
{
   public bool CouldChange = false;
   public int ItemSelectedIndex;
   private int PressNum;
   [NonSerialized] public GameObject ItemDescriptionPanel;
   [NonSerialized] public ItemDescriptionPanel itemDescriptionPanel;
   [HideInInspector]public Sprite ItemSprite;
   public int ChangeWithItemIndex;
   public GameObject[] NewItems;//升级后的道具
   public SEClipName WhenChangeSE;
   public ItemChangeRule[] itemChangeRules;
   public bool Reusable=false;
   public bool temp;
   public Sprite BigSprite;
    public bool Show= true;
   protected override void Start()
   {
      base.Start();
      if (BigSprite==null)
      {
         ItemSprite = transform.GetComponent<Image>().sprite;
      }
      else
      {
         ItemSprite = BigSprite;
      }


      itemDescriptionPanel = ItemDescriptionPanel.GetComponent<ItemDescriptionPanel>();
        if (!Show) { return; }
        ShowPanel();
   }

   protected override void OnBtnClick()
   {
      foreach (var VARIABLE in gameManger.Items)
      {
         if (VARIABLE!=null&&VARIABLE!=gameObject)
         {
            if (VARIABLE.GetComponent<ItemButton>().temp)
            {
               VARIABLE.SendMessage(nameof(Close));
            }
         }
      }
      PressNum++;
       temp = PressNum % 2 == 1; //获取当前按钮点击次数
      transform.GetChild(0).gameObject.SetActive(temp);
      //GridOutline.enabled = temp; //根据点击次数奇偶性设置物品栏描红开启或关闭
      var c=GameManager.gameManager.ItemSelecteditions[ItemSelectedIndex-1] = temp;//根据点击次数奇偶性设置物品效果启用或关闭
      if(!temp)
      {
         ShowPanel();//偶数次时物品展示面板中的贴图赋值，并将展示面板显示出来
      }

     
      
   }

   public void AfterUse()
   {
      PressNum++;
       temp = PressNum % 2 == 1; //获取当前按钮点击次数
      transform.GetChild(0).gameObject.SetActive(temp);
      var c=GameManager.gameManager.ItemSelecteditions[ItemSelectedIndex-1] = temp;//根据点击次数奇偶性设置物品效果启用或关闭
     
   }

   private void ShowPanel()
   {
       
      itemDescriptionPanel.BlackSprite = ItemSprite;
   
         var BlackButton =ItemDescriptionPanel.GetComponentInChildren<BlackButton>();
         BlackButton.Se = WhenChangeSE;
         BlackButton.CouldChange = CouldChange;
         BlackButton.CurrentItemIndex = ItemSelectedIndex;
         BlackButton.ChangeWithItemIndex = ChangeWithItemIndex;
         BlackButton.NewItems = NewItems;
         BlackButton.itemChangeRules = itemChangeRules;
         ItemDescriptionPanel.SetActive(false);
         ItemDescriptionPanel.SetActive(true);

    }

   /*private void OnDisable()
   {
  
         transform.parent.GetComponent<Image>().color=Color.clear;
      
         transform.parent.DetachChildren();
   }*/
   public void Close()
   {
      PressNum++;
      temp = PressNum % 2 == 1; //获取当前按钮点击次数
      transform.GetChild(0).gameObject.SetActive(temp);
      GameManager.gameManager.ItemSelecteditions[ItemSelectedIndex-1] = temp;//根据点击次数奇偶性设置物品效果启用或关闭
     
   }

   private void OnDestroy()
   {
      GameManager.gameManager.ItemSelecteditions[ItemSelectedIndex-1] = false;
      transform.parent.GetComponent<Image>().color=Color.clear;
     
   }
}
[System.Serializable]
public struct ItemChangeRule 
{
    public int changeWithItemIndex;
    public GameObject[] newItems;
}
