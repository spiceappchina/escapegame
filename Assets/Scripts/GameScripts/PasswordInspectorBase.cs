﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class PasswordInspectorBase : MonoBehaviour
{
    private List<bool> NeedTips;
    public PasswordType PasswordType;
    public bool PasswordSectionalType;
    public SEClipName ManagedObjOpen;
    public SEClipName PasswordRight;
    public int[] TipIndex;
    private Text Text;
    public float PasswordRightDelayTime;
    private int _Length;
    public GameObject ItemDescriptionPanel;
    public GameObject Item;
    private GameManager gameManger => GameManager.gameManager;
    public int DestoryItemIndex;
    public int[] DestoryItemIndexs;
    public bool CreateItem = false;
    public GameObject[] OverAnimObjs;
    public int Length
    {
        get { return _Length;}
        set
        {
            _Length = value;
            UpdateAndCheckPassword();
        }
    }
    //[NonSerialized] 
    public String Txt;
    public GameObject ManagedObj;
    public String StringPassword;
    private bool ContainText;

    //[NonSerialized] 
    public List<bool> BoolArrayPassword;
    public int PasswordLength;

    public List<GameObject> ManagedObjs;
    
    // Start is called before the first frame update


    protected virtual void Start()
    {

        Text = gameObject.GetComponent<Text>();
        ContainText = Text != null;
        InitNeedTips();
        InitBoolArrayPassword();
    }
    protected virtual void PasswordRightDelay()
    {
       
        GameManager.gameManager.SetInputActive(false);
        AudioManager.audioManager.EnvironmentalSoundPlay(PasswordRight);
        Invoke(nameof(OnPasswordYes), PasswordRightDelayTime);
        Invoke(nameof(SetTrue), PasswordRightDelayTime);
    }

    protected virtual void OnPasswordYes()
    {
        
        AudioManager.audioManager.SEPlay(ManagedObjOpen);
        if (ManagedObj!=null)
        {
            if(!GameManager.gameManager.DontChageActive.Contains(ManagedObj))
            {
                GameManager.gameManager.ObjList.Add(ManagedObj);
                GameManager.gameManager.SetBackButtonActive();
            }
            ManagedObj.SetActive(true);
        }
        foreach (var VARIABLE in ManagedObjs)
            {
                VARIABLE.SetActive(!VARIABLE.activeSelf);
                GameManager.gameManager.ObjList.Remove(VARIABLE);
                GameManager.gameManager.SetBackButtonActive();
            }
        foreach (var VARIABLE in OverAnimObjs)
        {
            VARIABLE.SendMessage("PlayRightAnim");
        }
        if (DestoryItemIndex!=0)
        {
            if (gameManger.Items[DestoryItemIndex - 1] != null)
            {
                DestroyImmediate(gameManger.Items[DestoryItemIndex - 1]);
            }

        }
        if (DestoryItemIndexs.Length!=0)
        {
            foreach (var item in DestoryItemIndexs)
            {
                if (gameManger.Items[item - 1] != null)
                {
                    DestroyImmediate(gameManger.Items[item - 1]);
                }
            }
        }
        if (CreateItem)
            {
                foreach (var VARIABLE in gameManger.Girds)
                {
                    if (VARIABLE.transform.childCount == 0)
                    {
                        VARIABLE.GetComponent<Image>().color=Color.white;
                        var TempItem =Instantiate(Item,VARIABLE.transform);
                        TempItem.GetComponent<ItemButton>().ItemDescriptionPanel = ItemDescriptionPanel;
                        var Index = TempItem.GetComponent<ItemButton>().ItemSelectedIndex-1;
                        gameManger.Items[Index] = TempItem;
                        transform.parent.gameObject.SetActive(false);
                        return;
                    }
                }
            }

           
            GameManager.gameManager.SetBackButtonActive();
            PasswordInit();
        
    }
    protected virtual void OnPasswordError()
    {
        if (!PasswordSectionalType)
        {
            Txt = "";
        }

        if (ContainText)
        {
            Text.text = Txt;
        }

    }

    
    protected virtual void UpdateAndCheckPassword()
    {

        switch (PasswordType)
        {
            case PasswordType.String:
                if (Txt.Length <= StringPassword.Length && ContainText)
                {
                    Text.text = Txt;
                }

                for (int i = 1; i < TipIndex.Length+1; i++)
                {
                    NeedTips[i-1]=GameManager.gameManager.PasswordTips[TipIndex[i- 1]-1];
                }

                if (NeedTips.Contains(false))
                {
                    if (Txt.Length >= StringPassword.Length)
                    {
                        OnPasswordError();
                    }
                }

                if (!PasswordSectionalType)
                {
                    if (Txt .Equals(StringPassword))
                    {
                       
                        PasswordRightDelay();
                    }

                    if (Txt.Length >= StringPassword.Length && Txt != StringPassword)
                    {
                        OnPasswordError();
                    }
                }
                else
                {
                    
                    if (!NeedTips.Contains(false)&&Txt.Contains(StringPassword))
                    {
                       
                        PasswordRightDelay();
                    }
                }

                break;
            case PasswordType.BoolArray:
                for (int i = 1; i < TipIndex.Length+1; i++)
                {
                    NeedTips[i-1]=GameManager.gameManager.PasswordTips[TipIndex[i- 1]-1];
                }

                if (NeedTips.Contains(false))
                {
                    return;
                }

                if (!BoolArrayPassword.Contains(false))
                {
                   
                    PasswordRightDelay();
                }

                break;
        }

    }

    private void InitBoolArrayPassword()
    { 
        if (PasswordType != PasswordType.BoolArray)
        {
            return;
        }

        BoolArrayPassword.Clear();
        for (int i = 0; i < PasswordLength; i++)
        {
            BoolArrayPassword.Add(false);
        }
    }

    private void PasswordInit()
    {
        switch (PasswordType)
        {
            case PasswordType.BoolArray:
                InitBoolArrayPassword();
                break;
            case PasswordType.String:
                Txt = "";
                if(ContainText){ Text.text = Txt;}
                break;
        }
    }

    protected virtual void OnDisable()
    {
        PasswordInit();
    }

    private void SetTrue()
    {
        GameManager.gameManager.SetInputActive(true);
    }

    private void InitNeedTips()
    {
        NeedTips=new List<bool>();
        for (int i = 0; i < TipIndex.Length; i++)
        {
            NeedTips.Add(false);
        }
    }


}

public enum PasswordType
{
    String,
    BoolArray
}
