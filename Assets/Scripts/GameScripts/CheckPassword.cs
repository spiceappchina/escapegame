﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CheckPassword : MonoBehaviour
{
    public SEClipName PasswordRight;
    public int TipIndex;
    public GameObject ManagedObj;
    protected bool EventOver = false; //让密码正确后触发的事件只执行一次
    public int PasswordLength;

    [NonSerialized] public List<bool> Password;

    // Start is called before the first frame update
    void Start()
    {
        Password = new List<bool>();
        for (int i = 0; i < PasswordLength; i++)
        {
            var password = false;
            Password.Add(password);
        }
    }

    // Update is called once per frame
    void Update()
    {

        CheckThePassword();
    }

    protected virtual void CheckThePassword()
    {

        if (!GameManager.gameManager.PasswordTips[TipIndex - 1])
        {
            return;
        }


        if (!Password.Contains(false))
        {
            OnPasswordYes();
        }
    }


    protected virtual void OnPasswordYes( )
    {
       
        if (EventOver)
        {
            return;
        }
        EventOver = true;
        GameManager.gameManager.SetInputActive(false);
        AudioManager.audioManager.SEPlay(PasswordRight);
        
       Invoke(nameof(ManagedObjShow),1f);
        
        
    }

    private void ManagedObjShow()
    {

        if (ManagedObj.activeSelf) 
        {
            GameManager.gameManager.ObjList.Remove(ManagedObj);
        }
        else
        {
            GameManager.gameManager.ObjList.Add(ManagedObj);
        }
       
        
        ManagedObj.SetActive(!ManagedObj.activeSelf);
        GameManager.gameManager.SetInputActive(true);
    }
}
