﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Stage24RotateButton01 : ButtonBase
{
    public int RightNum;
    public SEClipName FailSE;
    public Transform Button02;
    protected override void OnBtnClick()
    {
        var num = Button02.GetComponent<Stage24RotateButton>().Num;
        transform.DORotate(transform.eulerAngles + new Vector3(0, 0, -90), 0.5f, RotateMode.FastBeyond360)
            .OnPlay(() =>
            {
                GameManager.gameManager.SetInputActive(false);
                Button02.SendMessage("ButtonRotationInit");
            })
            .OnComplete(() =>
            {
                GameManager.gameManager.SetInputActive(true);
                if (num == RightNum)
                {
                    base.OnBtnClick();
                }
                else
                {
                    transform.rotation=Quaternion.Euler(Vector3.zero);
                    AudioManager.audioManager.SEPlay(FailSE);
                }
            });
    }
    private void OnEnable()
    {
        transform.rotation=Quaternion.Euler(Vector3.zero);
       
    }
}
